// das ist der command zum hot-reload, einfach ignorieren
// nodemon --exec "cls && javac Fahrkarte2.java && java Fahrkarte2" -w Fahrkarte2.java

import java.util.Scanner;

public class Fahrkarte2 {
  public static void main(String[] args) {
    Scanner tastatur = new Scanner(System.in);

    // dies ist ein double
    double zuZahlenderBetrag;
    // dies ist ein double
    double eingezahlterGesamtbetrag;
    // dies ist ein double
    double eingeworfeneMuenze;
    // dies ist ein double
    double rueckgabebetrag;

    /**
     * Mehr als 127 Tickets werden nicht verkauft, dann muss die Papierrolle ja
     * so oft gewechselt werden
     */
    byte ticketAmount;

    System.out.print("Zu zahlender Betrag (EURO): ");
    zuZahlenderBetrag = tastatur.nextDouble();

    System.out.print("Anzahl der Tickets (max. 127): ");
    ticketAmount = tastatur.nextByte();
    zuZahlenderBetrag = zuZahlenderBetrag * ticketAmount;

    // Geldeinwurf
    // -----------
    eingezahlterGesamtbetrag = 0.0;
    while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
      System.out
          .println("Noch zu zahlen: " + String.format("%.2f", zuZahlenderBetrag - eingezahlterGesamtbetrag) + " Euro");
      System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
      eingeworfeneMuenze = tastatur.nextDouble();
      eingezahlterGesamtbetrag += eingeworfeneMuenze;
    }

    // Fahrscheinausgabe
    // -----------------
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++) {
      System.out.print("=");
      try {
        Thread.sleep(250);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    System.out.println("\n\n");

    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
    rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    if (rueckgabebetrag > 0.0) {
      System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rueckgabebetrag) + " EURO");
      System.out.println("wird in folgenden Münzen ausgezahlt:");

      while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
      {
        System.out.println("2 EURO");
        rueckgabebetrag -= 2.0;
      }
      while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
      {
        System.out.println("1 EURO");
        rueckgabebetrag -= 1.0;
      }
      while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
      {
        System.out.println("50 CENT");
        rueckgabebetrag -= 0.5;
      }
      while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
      {
        System.out.println("20 CENT");
        rueckgabebetrag -= 0.2;
      }
      while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
      {
        System.out.println("10 CENT");
        rueckgabebetrag -= 0.1;
      }
      while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
      {
        System.out.println("5 CENT");
        rueckgabebetrag -= 0.05;
      }
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
        + "Wir wünschen Ihnen eine gute Fahrt.");
  }
}
