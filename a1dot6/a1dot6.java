// das ist der command zum hot-reload, einfach ignorieren
// nodemon --exec "cls && javac a1dot6.java && java a1dot6" -w a1dot6.java

public class a1dot6 {

  static Float round(Float f) {
    return Math.round(f * 100) / 100.0f;
  }

  static Float convertToCelsius(Float fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
  }

  public static void main(String[] args) {
    // inputs
    Float[] INPUTS = { -20f, -10f, 0f, 10f, 20f };
    String[] HEADERS = { "Fahrenheit", "Celsius" };

    // constants
    String HORIZONTAL_SEPERATOR = "-";
    String VERTICAL_SEPERATOR = "|";
    String SEPERATOR_INTERSECTION = "+";
    String PADDING_CHAR = " ";

    String[][] tableOutputs = new String[INPUTS.length][2];
    String[] rowStrings = new String[INPUTS.length + 2];

    // header row, darauf basiert die tabellen-breite
    rowStrings[0] = HEADERS[0] + PADDING_CHAR + VERTICAL_SEPERATOR + PADDING_CHAR + HEADERS[1];

    // zweite zeile, um header und content zu trennen
    rowStrings[1] = "";
    for (int i = 0; i < rowStrings[0].length(); i++) {
      if (i == (rowStrings[0].length()) / 2 + 1) {
        rowStrings[1] += SEPERATOR_INTERSECTION;
      } else {
        rowStrings[1] += HORIZONTAL_SEPERATOR;
      }
    }

    // convert input values zu celsius strings
    for (int i = 0; i < INPUTS.length; i++) {
      Float fahrenheit = INPUTS[i];
      Float celsius = round(convertToCelsius(fahrenheit));
      tableOutputs[i] = new String[] { fahrenheit.toString(), celsius.toString() };
    }

    // berechne wie breit eine cell sein muss, minus dem seperator
    int cellWidth = (rowStrings[0].length() - VERTICAL_SEPERATOR.length()) / 2;

    // erstelle die tabellen-zeilen
    for (int i = 0; i < tableOutputs.length; i++) {
      String[] row = tableOutputs[i];
      String rowOut = "";

      for (int j = 0; j < row.length; j++) {
        String cell = row[j];

        if (j == 0) {
          // linke zelle
          rowOut += cell;
          for (int k = 0; k < cellWidth - cell.length() + 2; k++) {
            rowOut += PADDING_CHAR;
          }
          rowOut += VERTICAL_SEPERATOR;
        } else {
          // rechte zelle
          for (int k = 0; k < cellWidth - cell.length() - 1; k++) {
            rowOut += PADDING_CHAR;
          }
          rowOut += cell;
        }
      }

      rowStrings[i + 2] = rowOut;
    }

    // zeilen-array printen
    for (String string : rowStrings) {
      System.out.println(string);
    }

  }

}