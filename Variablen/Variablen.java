/**
 * Variablen.java
 *
 * @author osommer
 */
public class Variablen {
  public static void main(String[] args) {
    /*
     * 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine
     * geeignete Variable
     */
    int counter;
    /*
     * 2. Weisen Sie dem Zaehler den Werte 25 zu und geben Sie ihn auf dem
     * Bildschirm aus.
     */
    counter = 25;
    System.out.println("Zaehlerstand = " + counter);
    /*
     * 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt eines Programms
     * ausgewaehlt werden. Vereinbaren Sie eine geeignete Variable
     */
    char selection;
    /*
     * 4. Weisen Sie dem Buchstaben den Werte 'C' zu und geben Sie ihn auf dem
     * Bildschirm aus.
     */
    selection = 'C';
    System.out.println("Auswahl = " + selection);
    /*
     * 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwert
     * notwendig. Vereinbaren Sie eine geeignete Variable
     */
    double lightspeed;
    /*
     * 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu und geben Sie sie
     * auf dem Bildschirm aus.
     */
    lightspeed = 2.9979E08;
    System.out.println("Grosse Zahl = " + lightspeed + " m/s");
    /*
     * 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung soll
     * die Anzahl der Mitglieder erfasst werden. Vereinbaren Sie eine geeignete
     * Variable und initialisieren sie diese sinnvoll.
     */
    short memberCount = 7;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus. */
    System.out.println("Anzahl der Mitglieder: " + memberCount);
    /*
     * 9. Fuer eine Berechnung wird die elektrische Elementraladung benoetigt.
     * Vereinbaren Sie eine geeignetes Attribut und geben Sie es auf dem Bildschirm
     * aus.
     */
    final double ELEMENTAL_CHARGE = 1.602E-19;
    System.out.println("Elektrische Elementarladung: " + ELEMENTAL_CHARGE + " C");
    /*
     * 10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
     * Vereinbaren Sie eine geeignete Variable.
     */
    boolean isPaid;
    /*
     * 11. Die Zahlung ist erfolgt. Weisen Sie der Variablen den entsprechenden Wert
     * zu und geben Sie die Variable auf dem Bildschirm aus.
     */
    isPaid = true;
    System.out.println("Rechnung wurde bezahlt:  " + isPaid);
  }
}
