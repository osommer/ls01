// das ist der command zum hot-reload, einfach ignorieren
// nodemon --exec "cls && javac WeltDerZahlen.java && java WeltDerZahlen" -w WeltDerZahlen.java

/**
 * Aufgabe: Recherchieren Sie im Internet !
 *
 * Sie dürfen nicht die Namen der Variablen verändern !!!
 *
 * Vergessen Sie nicht den richtigen Datentyp !!
 *
 *
 * @version 1.0 from 21.08.2019
 * @author << osommer >>
 */

public class WeltDerZahlen {

  public static void main(String[] args) {

    /*
     * *********************************************************
     *
     * Zuerst werden die Variablen mit den Werten festgelegt!
     ***********************************************************
     */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem
    final int anzahlPlaneten = 8;

    // Anzahl der Sterne in unserer Milchstraße
    final long anzahlSterne = 300_000_000_000L;

    // Anzahl der Sterne in unserer Sonne

    // Wie viele Einwohner hat Berlin?
    final int bewohnerBerlin = 3_000_000;

    // Wie alt bist du? Wie viele Tage sind das?

    final short alterTage = 7196;

    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    final int gewichtKilogramm = 190_000;

    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
    final int flaecheGroessteLand = 17_000_000;

    // Wie groß ist das kleinste Land der Erde?

    final double flaecheKleinsteLand = 0.49;

    /*
     * *********************************************************
     *
     * Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
     * ***********************************************************
     */

    System.out.println("anzahlPlaneten: " + anzahlPlaneten);
    System.out.println("anzahlSterne: " + anzahlSterne);
    System.out.println("bewohnerBerlin: " + bewohnerBerlin);
    System.out.println("alterTage: " + alterTage);
    System.out.println("gewichtKilogramm: " + gewichtKilogramm);
    System.out.println("flaecheGroessteLand: " + flaecheGroessteLand);
    System.out.println("flaecheKleinsteLand: " + flaecheKleinsteLand);

  }
}
